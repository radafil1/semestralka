\documentclass[12pt,a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}

\usepackage{graphicx}
\usepackage{svg}
\usepackage{pdfpages}

\usepackage{xcolor}
\usepackage[
	colorlinks,
	linkcolor={red!50!black},
	citecolor={blue!50!black},      
	urlcolor={blue!50!black}
]{hyperref}

\usepackage{fontspec}
% i'm definitely not using the excuse of writing the documentation
% for a program to use a hideous monospace font
\setmainfont{CascadiaCode}
\usepackage{anyfontsize}

\usepackage{geometry}
\usepackage{float}

\geometry{
  a4paper,
  left=20mm,
  top=20mm,
  right=20mm,
}

\begin{document}
\center
\begin{LARGE}
The Very Simple Tracker
\end{LARGE}

A program for making very simple tunes on the MZ\_APO board.

\raggedright

\vspace{2em}

Available at \url{https://gitlab.fel.cvut.cz/radafil1/semestralka}

\section*{Overview}
The program is used for creating simple audio tunes (also called tracks), or load such tunes
from some of the example files available in the source code repository. In order to achieve this,
it uses the LCD, LED and knob interfaces to communicate with the user. The user can either turn,
or press any of the three distinctly colored knobs.

\section*{Input modes}
There are three main input modes the user can encounter when interacting with the program.

\subsection*{Waiting mode}
Waiting mode is the simplest of the three modes, it just waits for the user to press any of the
three knobs on the MZ\_APO board. It is only used when an error message is displayed to the user
(for example when a music file's format cannot be determined).

\subsection*{"Classic" menu}
In a "classic" menu, the user can select one of the available options by \textbf{turning the blue
knob} and selecting it by \textbf{pressing it}. The position of currently selected option
in the whole menu is indicated by a left-to-right scrollbar on the LED line.

The user can also usually \textbf{press the middle
green} knob to go back to the previous menu, but this functionality is implemented by the specific
menus, so it might not be guaranteed to be present everywhere.

\newpage{}
\subsection*{Grid editing}
Grid editing is an input method used for editing the tones in a tune and also the properties of
the tune. A grid consists of fields (columns), which contain "cells" of various types.

Here's an example grid with an additional column for displaying row number on the left, which
cannot be edited:
\vspace{2em}

\includesvg[inkscapelatex=false,scale=2]{grid.svg}
\subsubsection*{The grid}
Users can navigate the Grid
editor with the \textbf{blue knob} for horizontal movement and the \textbf{red knob} for vertical
movement. When they navigate to the \hyperref[field_edit]{field} they want to edit, they can edit it by
\textbf{pressing the blue knob}. To exit this mode, users can \textbf{press the green knob}.
\subsubsection*{A field}
\label{field_edit}
The field, the user edits the individual cells. The only way the user can navigate while editing
a field is by exiting the editation of the individual cells by \textbf{pressing the red or the
blue knob}, which lets them edit the previous or next cell, respectively, or by \textbf{
  pressing the green knob} to quit editing the field altogether. Otherwise the field editation
ends, when the user exits editing a cell to the side, where there are no more cells.

\newpage{}
\subsubsection*{Cell}
The value of a cell can be changed by 1 by \textbf{turning the blue knob}. They can also by changed
by 10 (this is useful for number cells with large ranges) by \textbf{turning the red knob}. User can
end the editing of the cell by \textbf{pressing either of the knobs}. Which knob they pressed
determines, where they move in the field the cell is a part of. As in the "classic" menu,
the position of the current value in the pool of all values is indicated on the LED line.

\section*{Program structure}
This section tries to describe the structure of the program from both the users and programmers
perspective, as they are very similar.
\subsection*{Main menu}
The main menu (\verb|src/main_menu.c|) provides the user with the option to either load a file
from a specified directory (which is passed to the program as an argument), or to create a new
song. When creating a new tune, the user is prompted, which sound engine they wish to use (though
there's only one ATM). The generic menu functionality is implemented in the \verb|src/strings_menu.c|
file.

\subsection*{SEngine menu}
The only available sound engine \verb|SEngine| (found in \verb|src/SEngine_ui.c|
and \verb|src/SEngine.c|) has a main menu, in which the user can choose to edit, play or
modify the properties of a tune. The tune properties available to change are the BPM (tells
the engine how many beats per minute it should play) and the number of tones there
are. When a user creates a new tune, they are prompted to specify its properties first.

\subsection*{SEngine editor}
The \verb|SEngine| editor is a basic grid editor with three editable fields. The first field
has three cells and lets the user to choose which note to play. The second and third field
have just one number cell. The second field is the relative pulse width of the tone, the third
is the duration of the note in engine beats.
It uses the generic grid editor from \verb|src/grid_edit.c|
\newpage{}
Here's a very stylized illustration of how a single row in the SEngine editor might
look\footnote{It cannot actually look like this, because cells don't have borders and
I also have terrible taste.}:
\vspace{2em}

\includesvg[inkscapelatex=false,scale=2]{SEngine}

\subsection*{SEngine player}
The SEngine player is just reusing the same grid the editor uses, but highlights the row currently
playing. When playing a song, it can be ended by \textbf{pressing and holding the green knob}.
It checks for the green knob being pressed only when the row changes.

\subsection*{The rest}
All menus and the editor use the generic grid, field and cell definitions from \verb|inc/input_cell.h|,
\verb|input_field.h| and \verb|field_grid.h| with functions to render and modify them defined
in the corresponding files in the \verb|src/| directory. They use \verb|src/graphics.c|
for rendering to the MZ\_APO LCD and LED line and \verb|src/input_loop.c| for processing
inputs from the user.

\section*{SEngine file format}
The SEngine can open a simple music file, but it must follow a strict set of rules. It has
to have the word "SEngine" on the first line (and only that) and the BPM and total number
of track lines on the next two (both positive integers). The rest of the file follows the
same structure as the engines editor grid, but the fields are separated by spaces.

Here's an example encoding the opening notes from Ode to joy with alternating
pulse width, to show the difference in sound:
\begin{verbatim}
SEngine
600
015
F#4 16 02
F#4 17 02
G-4 18 02
A-4 19 02
A-4 20 02
G-4 19 02
F#4 12 02
E-4 08 02
D-4 05 02
D-4 04 02
E-4 08 02
F#4 12 02
F#4 15 03
E-4 13 01
E-4 12 04
\end{verbatim}
\end{document}
