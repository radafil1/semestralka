# The Very Simple Tracker

## ABOUT
This is a very simple tracker (so simple it doesn't even have a name) for the MZ\_APO board.
It can be used to make and play simple tunes.

## COMPILING AND RUNNING

### MANUALLY
Compiling the program is done by running `make`, which creates the `VerySimpleTracker` executable
for the MZ\_APO board, which can then be run on the board. Running it requires one argument,
which is the directory containing music files (or no files at all).
#### EXAMPLE
After running `make` and copying the executable to the board, one might run the program
with `/tmp/filip/VerySimpleTracker /tmp/filip/examples/`, where /tmp/filip/VerySimpleTracker is the
path to the executable
(on the board) and where `/tmp/filip/examples/` is a directory containing some music files.

### AUTOMATICALLY
If there's a direct IP connection between the computer which is compiling the program and the
MZ\_APO board, one can compile and run the program by running `TARGET_IP=???.???.???.??? make run`,
where ???.???.???.??? is the IP address of the MZ\_APO board.

### CLEANING
If removing all object files is needed, `make clean` can be run to remove them all at once and
if rebuilding should be done right after cleaning, one can use `make clean all` to clean and rebuild,
or `make clean all run` to rebuild and run.

## MUSIC FILES
The music files are located in the `examples/` directory, which is copied to the board when doing
`make run`. The music files have to have the name of the sound engine (just "SEngine") as the first
line, followed by the engine-soecific track properties and the table of tones.

## LICENSING
Apart from the files in the lib/ directory, the corresponding header files in the inc/ directory,
and the Makefile, which are copied from the APO semestral work template and are under their
original licenses, feel free to use, modify and do whatever you like with all the other files.
