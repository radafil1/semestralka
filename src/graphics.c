/*******************************************************************
  
  graphics.c  - simple interface for working with graphical output

  This file implements basic display and LED line functionality,
  shielding the other parts of the program from interacting with
  them directly

 *******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#include "font_types.h"
#include "graphics.h"

// frame buffer
static unsigned short *fb = NULL;
static unsigned char *parlcd_mem_base;
static unsigned char *spilled_mem_base;

void display_init (void) {
  if (fb != NULL) return;
  fb  = (unsigned short *)malloc(320*480*2);
  spilled_mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (parlcd_mem_base==NULL || spilled_mem_base == NULL)
    exit(1);
  // this breaks some displays, but seems to work on the others (if they're
  // initialized by some other program, but they probably will?)

  //parlcd_hx8357_init(parlcd_mem_base);
}

void display_free (void) {
  free(fb);
}

void display_refresh (void) {
  int ptr = 0;

  if (fb==NULL)
    display_init();

  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  for (ptr = 0; ptr < 240*320 ;) {
    parlcd_write_data2x(parlcd_mem_base, *(((uint32_t*)fb) + ptr++));
  }
}

void display_clear (void) {
  int ptr;
  if (fb==NULL)
    display_init();
  for (ptr = 0; ptr < 480*320 ;ptr++) {
    fb[ptr] = 0;
  }
}


void draw_pixel (int x, int y, unsigned short color) {
  if (fb==NULL)
    display_init();

  if (x>=0 && x<480 && y>=0 && y<320) {
    fb[x+480*y] = color;
  }
}

void draw_rect (int x, int y, int width, int height, unsigned short color) {
  int row, col;
  for (row = 0; row < height; row++) {
    for (col = 0; col < width; col++) {
      draw_pixel(x+col, y+row, color);
    }
  }
}

int char_width(font_descriptor_t* fdes, int ch) {
  int width = 0;
  if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
    ch -= fdes->firstchar;
    if (!fdes->width) {
      width = fdes->maxwidth;
    } else {
      width = fdes->width[ch];
    }
  }
  return width;
}

void draw_char (int x, int y, char ch, text_style_t *style) {
  short int width, height;
  int row, col;
  width = char_width(style->fdes, ch);
  if (!width) return;
  height = style->fdes->height;
  ch -= style->fdes->firstchar;
  for (row = 0; row < height; row++) {
    font_bits_t r = style->fdes->bits[ch*height+row];
    int mask = 1 << 15;
    for (col = 0; col < width; col++) {
      if (r&mask) {
        draw_rect(x+col*(style->size),y+row*(style->size),style->size,style->size,style->color0);
      }
      else if (style->opacity1) draw_rect(x+col*(style->size),y+row*(style->size),
                                      style->size,style->size,(unsigned short)style->color1);
      mask >>= 1;
    }
  }
}

// Print a zero-terminated string, wrap 0 doesn't wrap, otherwise wraps wrap pixels,
// len limits the number of chars to be printed (-1 for unlimited)
void draw_str (int x, int y, const char *str, unsigned int wrap,
                int len, text_style_t *style) {
  int x_offset, y_offset, width;
  x_offset = y_offset = 0;
  if ((wrap>0&&wrap<style->fdes->maxwidth)||str==NULL||!(*str)) return; // Won't print anything
  width = char_width(style->fdes,*str)*style->size;
  while (len--) {
    draw_char(x+x_offset,y+y_offset,*(str++),style);
    x_offset += width;
    if (!(*str)) return;
    width = char_width(style->fdes, *str)*style->size;
    if (wrap&&x_offset+width>wrap) {
      x_offset = 0;
      y_offset += style->fdes->height;
    }
  }
}

void scroll_bar (int current, int total, int width) {
  uint32_t val_line = 0xFFFFFFFF << (32-width);
  val_line >>= (int) ((double) current/total * (32-width));
  *(volatile uint32_t*)(spilled_mem_base + SPILED_REG_LED_LINE_o) = val_line;
}

void progress_bar (int current, int total) {
  uint32_t val_line = 0xFFFFFFFF << (unsigned int)(32-((double)current/total)*32.0);
  *(volatile uint32_t*)(spilled_mem_base + SPILED_REG_LED_LINE_o) = val_line;
}

void led_clear (void) {
  *(volatile uint32_t*)(spilled_mem_base + SPILED_REG_LED_LINE_o) = 0;
}
