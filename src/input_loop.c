/*******************************************************************
  
  input_loop.c  - the sort-of universal input handling

  Program implements some useful functions for getting inputs from
  the MZ_APO knobs interface (it takes care of the physical memory
  access). The main functionality is in the input_loop() procedure,
  which is a kind of a state machine, that upon being called and
  registering input change calls the handler procedures, which can
  be set with the input_loop_set_handlers() function.
 *******************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"
#include "knobs.h"

#include "input_loop.h"

static unsigned char *knobs_mem_base = NULL;
static knobs_t knobs;
static knobs_t knobs_prev = {255,255,255,1,1,1,31};
static short dr, dg, db;

static event_handler_t event_handler_list[12];


static void void_function (void) {}

static void exit_function (void) {
  exit(0);
}

static void init_loop (void) {
  int i;
  knobs_mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  knobs_prev = knobs_read(knobs_mem_base);
  if (knobs_mem_base == NULL)
    exit(1);
  for (i=0;i<12;i++) event_handler_list[i] = &void_function;
  event_handler_list[4] = &exit_function;
}

knobs_t input_knobs_read (void) {
  if (knobs_mem_base==NULL)
    init_loop();
  return knobs_read(knobs_mem_base);
}

void wait_for_click (void) {
  if (knobs_mem_base==NULL)
    init_loop();

  struct timespec loop_delay = {.tv_sec=0, .tv_nsec = 20 * 1000 * 1000};
  knobs_prev = knobs_read(knobs_mem_base);
  while(1) {
    knobs = knobs_read(knobs_mem_base);
    if ((knobs.rc != knobs_prev.rc) && (knobs.rc == 0))
      return;
    if ((knobs.gc != knobs_prev.gc) && (knobs.gc == 0))
      return;
    if ((knobs.bc != knobs_prev.bc) && (knobs.bc == 0))
      return;
    knobs_prev = knobs;
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);

  }
}

void input_loop_set_handlers (event_handler_t *event_handlers) {
  int i;
  if (knobs_mem_base==NULL)
    init_loop();
  knobs_prev = knobs_read(knobs_mem_base);
  for (i=0;i<12;i++)
    if(event_handlers[i]==NULL)
      event_handler_list[i] = &void_function;
    else
      event_handler_list[i] = event_handlers[i];
}

void input_loop_get_handlers (event_handler_t *event_handlers) {
  int i;
  if (knobs_mem_base==NULL)
    init_loop();
  knobs_prev = knobs_read(knobs_mem_base);
  for (i=0;i<12;i++)
    event_handlers[i] = event_handler_list[i];
}

void input_loop () {
  knobs_t cmp;
  if (knobs_mem_base==NULL)
    init_loop();

  knobs = knobs_read(knobs_mem_base);
  dr = knobs_prev.r - knobs.r;
  dg = knobs_prev.g - knobs.g;
  db = knobs_prev.b - knobs.b;
  cmp = uint_to_knobs(knobs_comp(knobs_prev,knobs));

  if (cmp.rc) {
    if (knobs.rc) event_handler_list[1](); // Red button DOWN
    else event_handler_list[0](); // Red button UP
  }
  if (cmp.gc) {
    if (knobs.gc) event_handler_list[5]();
    else event_handler_list[4]();
  }
  if (cmp.bc) {
    if (knobs.bc) event_handler_list[9]();
    else event_handler_list[8]();
    //exit(0); Let this be a reminder not to put exits anywhere for debugging (2 HOURS!!)
  }
  // this way of reading the knobs is not ideal, but it'll have to do for now...
  // (little did I know, I would never come back to this, but even though it sucks
  //  pretty hard, I never really had a problem with it in the whole 5 minutes I spent
  //  testing the program...)
  if ((knobs.r&0xFC) > (knobs_prev.r&0xFC)) {
    if ((cmp.r&0x80)&&(knobs.r&0xC0)==0xC0) event_handler_list[3]();
    else event_handler_list[2]();
  }
  else if ((knobs.r&0xFC) < (knobs_prev.r&0xFC)) {
    if ((cmp.r&0x80)&&(knobs.r&0xC0)==0x0) event_handler_list[2]();
    else event_handler_list[3]();
  }
  if ((knobs.g&0xFC) > (knobs_prev.g&0xFC)) {
    if ((cmp.g&0x80)&&(knobs.g&0xC0)==0xC0) event_handler_list[7]();
    else event_handler_list[6]();
  }
  else if ((knobs.g&0xFC) < (knobs_prev.g&0xFC)) {
    if ((cmp.g&0x80)&&(knobs.g&0xC0)==0x0) event_handler_list[6]();
    else event_handler_list[7]();
  }
  if ((knobs.b&0xFC) > (knobs_prev.b&0xFC)) {
    if ((cmp.b&0x80)&&(knobs.b&0xC0)==0xC0) event_handler_list[11]();
    else event_handler_list[10]();
  }
  else if ((knobs.b&0xFC) < (knobs_prev.b&0xFC)) {
    if ((cmp.b&0x80)&&(knobs.b&0xC0)==0x0) event_handler_list[10]();
    else event_handler_list[11]();
  }
  knobs_prev = knobs;
}
