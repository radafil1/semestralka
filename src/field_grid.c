/*******************************************************************
  
  field_grid.c  - editing and rendering of a field-based table

  This file implements files for working with the field_grid, defined
  in inc/field_grid.h

 *******************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "font_types.h"

#include "graphics.h"
#include "input_cell.h"
#include "input_field.h"
#include "field_grid.h"
#include "file_utils.h"

// set grid from row row to default values (for enlarging the values array)
void grid_default (field_grid_t *grid, int row) {
  int f, c, r;
  short *values = grid->values + row*(grid->ncells);
  short defaults[grid->ncells];
  r = 0;
  for (f=0; f<grid->nfields; f++) {
    for (c=0; c<grid->fields[f].ncells; c++) {
      defaults[r++] = grid->fields[f].cells[c].value;
    }
  }
  for (r=row; r<grid->nrows; r++) {
    for (c=0; c<grid->ncells; c++) {
      values[r*(grid->ncells)+c] = defaults[c];
    }
  }
}

// load grid from a file pointed to by fp, we don't really do
// any checks here, so the file better be correctly done
//
// skip chars in between fields (for human readable files)
int load_grid (field_grid_t *grid, int row, FILE *fp, int skip) {
  char line[256];
  char *cp;
  int r, f;
  short *values = grid->values + row*(grid->ncells);
  for (r=row; r<grid->nrows; r++) {
    fread_line(fp, line);
    cp = line;
    for (f=0; f<grid->nfields; f++) {
      grid->fields[f].values = values;
      load_input_field(grid->fields+f, cp);
      cp += grid->fields[f].length + skip;
      values += grid->fields[f].ncells;
    }
  }
  return 0;
}

// *selected is an array of pairs of coordinates (row,col) of selected
// fields in the grid
void render_grid (field_grid_t *grid, int xp, int yp, int row,
  unsigned short nselected, short *selected) {
  int x, y, height;
  int c, cells;
  short r;
  input_field_t *field;
  
  if (!grid->v_step) {
    for (c=0;c<grid->nfields;c++) {
      render_field(grid->fields+c, 4000, 4000, -2); // to trigger height recalculation
      grid->v_step = grid->fields[c].height > grid->v_step ? 
        grid->fields[c].height : grid->v_step;
    }
    grid->v_step += grid->v_gap;
  }
  // first field should be biggest, maybe we can calculate the max, or add it to the structure
  // but idk, we don't need that now...
  
  // adjusting the height if we're out of rows
  height = grid->nrows - row;
  height = height < grid->height ? height : grid->height;
  
  // rendering counting rows
  if (grid->counting) {
    y = yp;
    r = row;
    x = xp;
    grid->fields[grid->nfields].values = &r;
    for (c = 0; c<height; c++) {  
      render_field(grid->fields+grid->nfields, x, y, -2);
      y += grid->v_step;
      r++;
    }
    xp += grid->fields[grid->nfields].width + grid->h_gap;
  }
  
  x = xp;
  cells = 0;
  for (c = 0; c<grid->nfields; c++) {
    y = yp;
    field = grid->fields+c;
    for (r=0; r<height; r++) {
      field->values = grid->values+(grid->ncells*(row+r))+cells;
      render_field(field, x, y, -2);
      y += grid->v_step; 
    }
    x += field->width + grid->h_gap;
    cells += field->ncells;
  }

  for (r=0;r<2*nselected;r+=2) {
    if (selected[r]<row||selected[r]>row+height) continue;
    x = xp;
    cells = 0;
    for (c=0;c<selected[r+1];c++) {
      x += grid->fields[c].width + grid->h_gap;
      cells += grid->fields[c].ncells;
    }
    (grid->fields+selected[r+1])->values = grid->values+
      grid->ncells*(selected[r])+cells;
    render_field(grid->fields+selected[r+1], x, yp + (grid->v_step)
      *(selected[r]-row), -1);
  }
}
