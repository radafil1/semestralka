/*******************************************************************
  
  SEngine_ui.c  - the Simple Engine, for playing uncomplicated sounds

  Implementation of mostly the user-oriented part of a sound-playing
  part of the program, also contains functionality for loading from
  files

 *******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#include "font_types.h"

#include "knobs.h"
#include "graphics.h"
#include "input_cell.h"
#include "input_field.h"
#include "field_grid.h"
#include "grid_edit.h"
#include "str_utils.h"
#include "strings_menu.h"
#include "input_loop.h"
#include "file_utils.h"


#include "engine.h"
#include "SEngine.h"

// BPM(0) and number of rows(1)
static short properties[] = {0,0};

static text_style_t styl[6] = {
//  colors                 opacities       size font
  {0xFDDF, 0xF0A6, 0xF00F, 0x01, 0x01, 0x01, 3, &font_rom8x16},
  {0x0FF0, 0xF026, 0xF52F, 0x01, 0x00, 0x01, 3, &font_rom8x16},
  {0xD7ED, 0xFD56, 0x1111, 0x01, 0x01, 0x01, 3, &font_rom8x16},
  {0xADFF, 0xC077, 0xC077, 0x01, 0x01, 0x01, 3, &font_rom8x16},
  {0xFFFF, 0xFD56, 0x1111, 0x01, 0x01, 0x01, 3, &font_rom8x16},
  {0xFFFF, 0xFD56, 0x1111, 0x01, 0x01, 0x01, 3, &font_rom8x16}
};

/*
  This is the structure of the SEngine data, it is a field
  grid with 3 fields:
  
  Tone field  Pulse width field   Duration field

  The tone field contains three cells, one for choosing the
  note, one for choosing shifting by a semitone with b or #
  and one for choosing an octave

  Pulse width field is 0 - 32 scale of pulse width relative to
  the period (0 - very short pulse 32 - full pulse)

  Duration field is the tone duration in beats
*/

static input_cell_t prot[6] = {
  // note input cells
  {2, -1, 6, -1, 1, "-CDEFGAB", NULL},
  {2, -1, 1, 0, 1, "b-#", NULL},
  {2, -4, 4, 0, 1, "012345678", NULL},
  // filling cell
  {1, 0, 32, 16, 2, "", NULL},
  // duration cell
  {1, 1, 99, 1, 2, "", NULL},
  // counting cell
  {1, 0, 999, 0, 3, "", NULL},
};

static input_field_t fields[] = {
  {3, 3, 0, 0, 12, 3,  2, 0, prot, NULL, styl},
  {1, 2, 0, 0, 8, 3,  2, 0, prot+3, NULL, styl},
  {1, 2, 0, 0, 8, 3,  2, 0, prot+4, NULL, styl},
  // counting field
  {1, 3,0, 0, 20, 3,  2, 0, prot+5, NULL, styl+3}
};

static field_grid_t grid = {3, 5, 5, 0, 0, 0, -2, 0, 1, fields, NULL};


// play a song, this function is here because it uses the SEngine field grid...
void SEngine_play (short *rows, int nrows, unsigned int bpm) {
  int row;
  knobs_t knobs;
  short selection[6] = {0,0,0,1,0,2};
  struct timespec loop_delay = {.tv_sec=0, .tv_nsec = 0};
  
  grid.values = rows;
  grid.nrows = nrows;
  display_clear();
  for (row=0;row<nrows;row++) {
    int secdelay =  *(rows+row*SENGINE_ROW_WIDTH+4)*(60.0/bpm);
    loop_delay.tv_sec = secdelay;
    loop_delay.tv_nsec = (*(rows+row*SENGINE_ROW_WIDTH+4)*(60.0/bpm)-secdelay)/0.000000001;
    
    // using display refresh here as a divider of the notes (bpm will not be accurate, but whatever)
    selection[0] = row;
    selection[2] = row;
    selection[4] = row;
    display_clear();
    render_grid(&grid, 40, 5, row ? row-1 : 0, 3, selection);
    display_refresh();
    progress_bar(row, grid.nrows);

    SEngine_play_row(rows+row*SENGINE_ROW_WIDTH);
    // Stuff to do
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    SEngine_reset_pwm();
    knobs = input_knobs_read();
    if (knobs.gc) break;
  }
  led_clear();
  SEngine_reset_pwm();
}


// song properties selection menu
void SEngine_properties (void) {
  int row;
  char string[] = "BPM\0Beats total\0";
  char **strings = make_str_array(string, 2);
  input_cell_t cells[] = {
    {1, 1, 999, 100, 3, NULL, NULL},
    {3, 0,   1,   0, 11, "", strings}
  };
  input_field_t menu_fields[] = {
    {1, 3, 0, 0, 10, 5, 2, 4, cells, NULL, styl},
    {1, 11, 0, 0, 10, 5, 2, 4, cells+1, NULL, styl}
  };
  field_grid_t menu_grid = {1, 1, 2, 2, 0, 5, 10, 0, 1, menu_fields, properties};
  display_clear();
  field_grid_edit(&menu_grid, 20, 10, 0);
  free(strings);
  if (grid.values == NULL) {
    grid.values = (short*) malloc(properties[1]*grid.ncells*sizeof(short));
    grid.nrows = properties[1];
    row = 0;
  } else {
    row = grid.nrows;
    grid.values = (short*) realloc(grid.values, properties[1]*grid.ncells*sizeof(short));
    grid.nrows = properties[1];
  }
  if (grid.values == NULL)
    exit(1);
  grid_default(&grid,row);
}

// the main engine menu
void SEngine_song_menu (void) {
  char string[] = "Edit song\0Change properties\0Play song\0Exit\0";
  char **strings = make_str_array(string, 4);
  int choice = 0;

  if(grid.values==NULL) {
    SEngine_properties();
  }
  
  display_clear();
  input_cell_t cell = {3, 0, 0, 0, 17, NULL, NULL}; // for width
  input_field_t menu_field = {1, 17, 0, 0, 10, 5, 2, 4, &cell, NULL, styl};
  field_grid_t menu_grid = {1, 1, 5, 0, 0, 5, 10, 0, 0, &menu_field, NULL};
  while (choice!=3) {
    choice = strings_menu(20, 10, strings, 4, &menu_grid);
    if (choice == -1) break;
    switch (choice) {
      case 0:
        display_clear();
        field_grid_edit(&grid, 70, 10, 0);
        display_clear();
        break;
      case 1:
        SEngine_properties();
        break;
      case 2:
        SEngine_play(grid.values, grid.nrows, properties[0]);
        display_clear();
        break;
      default:
        break;
    }
  }
  free(grid.values);
}

void SEngine_new (void) {
  SEngine_song_menu();
}

void SEngine_open (FILE *file) {
  char line[256];

  input_cell_t cell = {1, 1, 999, 100, 3, NULL, NULL};
  fread_line(file, line);
  if (load_input_cell(&cell, properties,line)) {
    fclose(file);
    return;
  }

  fread_line(file, line);
  if (load_input_cell(&cell, properties+1,line)) {
    fclose(file);
    return;
  }
  
  grid.values = (short*) malloc(properties[1]*grid.ncells*sizeof(short));
  grid.nrows = properties[1];
  
  if (grid.values == NULL)
    exit(1);

  load_grid(&grid, 0, file, 1);
  fclose(file);

  SEngine_song_menu();
}
