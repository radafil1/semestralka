/*******************************************************************
  
  file_utils.c  - some useful functions for working with files

 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

#include "str_utils.h"


// We're going for the simplest of the simplest solutions
static int filter (const struct dirent *file) {
  if (file->d_type == DT_REG)
    return 1;
  return 0;
}

// we take the number of files by reference
char *get_dir (char *dir, int *num) {
  int n, len, f, ch;
  char *string;
  struct dirent **files;
  
  n = scandir(dir, &files, filter, alphasort);
  if (n==-1) {
    *num = -1;
    return NULL;
  }
  
  len = 0;
  for (f=0; f<n; f++) {
    string = files[f]->d_name;
    while(*(string++)) {
      len++;
    }
    len++;
  }
  // remember to free
  string = (char*) malloc(len*sizeof(char));
  if (string==NULL)
    exit(1);
  
  ch = 0;
  for (f=0; f<n; f++) {
    ch += str_copy(string+ch,files[f]->d_name);
    free(files[f]);
  }
  
  free(files);
  
  *num = n;
  return string;
}

// read until newline found into a null-terminated string
int fread_line (FILE *fp, char *buff) {
  int read;
  int total = 0;
  while (1) {
    read = fgetc(fp);
    if (read==EOF||read=='\n')
      break;
    *(buff++) = (char) read;
    total++;
  }
  *buff = '\0'; // we assume we're gonna process the line as a string
  if (read!=1) return -1;
  return ++total;
}
