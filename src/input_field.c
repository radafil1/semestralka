/*******************************************************************
  
  input_field.c  - an organized group of input cells

  This file implements functions for modifying and displaying the
  input fields (see inc/input_field.h for definition), similar
  to input_cell.c, except it's more cells aligned next to each
  other horizontally

 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "font_types.h"

#include "graphics.h"
#include "knobs.h"
#include "input_loop.h"
#include "input_cell.h"
#include "input_field.h"


// returns to what side we exited (-1 by pressing previous, 1 by next)
int get_input_field (input_field_t *field, int x, int y) {
  int c, ct;
  int x_offset, y_offset;
  if (field->ncells<=0) return 1;

  x_offset = field->padding_h+field->border_width;
  y_offset = field->padding_v+field->border_width;
  c = 0;
  while (1) {
    render_field(field, x, y, c);
    ct = get_input_cell(field->cells+c,field->values+c, x+x_offset, y+y_offset, field->styles+2);
    if (ct==-1) {
      if (!c) return -1;
      c--;
      x_offset = x_offset - field->styles[2].fdes->maxwidth * 
                 field->cells[c].length * field->styles[2].size - field->space;
    } else if (ct==1){
      if (++c==field->ncells) return 1;
      x_offset = x_offset + field->styles[2].fdes->maxwidth *
                 field->cells[c-1].length * field->styles[2].size + field->space;
    } else {
      return 0;
    }
  }
}


// loading the contents of a field from a string
int load_input_field (input_field_t *field, char *string) {
  int c;
  for (c=0; c<field->ncells; c++) {
    load_input_cell(field->cells+c, field->values+c, string);
    string += field->cells[c].length;
  }
  return 0;
}

// selection -2 (not selected), -1 selected field, 0..n selected cell (field is selcted)
void render_field (input_field_t *field, int x, int y, int selection) {
  int c, width, height;
  int sel = selection>=-1;

  if (field->width<=0) get_field_dimensions(field);
  if (field->values == NULL) return;
  // drawing background and border
  draw_rect(x,y, field->width, field->height,
          field->styles[2].color2*field->styles[2].opacity2);
  draw_rect(x+field->border_width, y+field->border_width, 
      field->width-2*field->border_width, field->height-2*field->border_width,
      (field->styles+sel)->color2*(field->styles+sel)->opacity2);

  // reusing variables for offsetting
  width  = field->padding_h+field->border_width;
  height = field->padding_v+field->border_width;
  for (c=0;c<field->ncells;++c) {
    render_cell(field->cells+c,field->values+c, x+width, y+height,
                field->styles+(sel+(c==selection)));
    width += field->cells[c].length * field->styles[0].fdes->maxwidth
             * field->styles[0].size + field->space;
  }
}


// this wasn't actually used anywhere, so idk if it works
/*
void reset_field (input_field_t *field) {
  int c;
  for (c=0;c<field->ncells;++c) {
    reset_cell(field->cells+c,field->values+c);
  }
}*/

// helping function to determine the dimensions of a field to be rendered
void get_field_dimensions (input_field_t *field) {
  int c, width, height;

  height = field->styles[0].fdes->height * field->styles->size;
  width  = 0;
  for (c=0;c<field->ncells;c++) {
    width += field->styles->fdes->maxwidth * field->styles->size
              * field->cells[c].length + field->space;
  }
  width -= field->space;
  field->width = width+2*(field->padding_h+field->border_width);
  field->height= height+2*(field->padding_v+field->border_width);
}
