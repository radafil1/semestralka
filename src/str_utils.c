/*******************************************************************
  
  str_utils.c  -  some useful functions to work with char arrays and
                  strings

 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>


// compare two strings (excuse me)
int str_comp (char *s1, char *s2) {
  while (*(s1++)==*(s2++)) if (!*s1) return !*s2 ? 1 : 0;
  return 0;
}


// we need the number of chars
int str_copy(char *dest, char *src) {
  int len = 0;
  while (*src) {
    *(dest++) = *(src++);
    len++;
  }
  *(dest) = '\0';
  return ++len;
}

// get first occurence of char in a string (null-terminated char array)
int char_index_str (char *array, char ch) {
  char *arr = array;
  do {
    if (*arr == ch) return arr - array;
  } while (*(arr++));
  return -1;
}

// when char array is not a string...
int char_index (char *array, char ch, int n) {
  int i;
  for (i = 0;i < n; i++) {
    if (array[i] == ch) return i;
  }
  return -1;
}

// find the index of a string in a string array
int str_index (char **strings, char *str, int n) {
  int i;
  for (i = 0;i < n; i++) {
    if (str_comp(strings[i],str)) return i;
  }
  return -1;
}


// takes a single pointer with null-separated strings
// and turns it into a 2d array of string pointers
// REMEMBER TO FREE!!! (note to self)
char **make_str_array (char *string, int n) {
  int i;
  char **strings = (char **) malloc(n*sizeof(char*));
  if (strings==NULL)
    exit(1);

  for (i=0;i<n;i++) {
    strings[i] = string;
    while (*(string++)) {
      continue;
    }
  }
  
  return strings;
}


// check if the end of filename is equal to extension
// ok, this function didn't get used, but it might be useful?
int has_extension ( char *filename, char *extension) {
  char *ext = extension;
  do {
    if (*ext==*filename) ext++;
    else ext = extension;
  } while (*(++filename));
  return *ext ? 0 : 1;
}
