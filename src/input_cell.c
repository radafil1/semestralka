/*******************************************************************
  
  input_cell.c  - the building stone of graphical input

  This file implements functions and procedures for working with,
  displaying and modifying (by the user) of the input_cell_t
  structure

 *******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "serialize_lock.h"
#include "mzapo_phys.h"
#include "font_types.h"

#include "str_utils.h"
#include "graphics.h"
#include "knobs.h"
#include "input_loop.h"
#include "input_cell.h"

static void input_cell_up (void);
static void input_cell_down (void);
static void input_cell_up_big (void);
static void input_cell_down_big (void);
static void input_cell_next (void);
static void input_cell_prev (void);

// for input_loop
static event_handler_t event_handlers[12] = {
  &input_cell_prev, NULL, &input_cell_up_big, &input_cell_down_big,
  &input_cell_next, NULL, NULL, NULL,
  &input_cell_next, NULL, &input_cell_up, &input_cell_down};

// static values for editing
static int cont = 0; // Indicates 1 next cell -1 prev cell (for usage in fields)
static short *value;    // Used for storing the value of the current cell

static input_cell_t* prot;
static int x, y;
text_style_t *style;

// get input from user
// Returns 1 (next) or -1 (previous)
int get_input_cell (input_cell_t *prototype, short *val,
                     int xp, int yp, text_style_t *styl) {
  
  struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 20 * 1000 * 1000};
  event_handler_t event_handlers_prev[12];
  
  if (prototype==NULL) return 0; // IDK, if these checks are neccessary
  value = val;
  prot = prototype;
  if (*value < prot->min || *value > prot->max) *value = prot->value;
  input_loop_get_handlers(event_handlers_prev); // Save present input handlers
  input_loop_set_handlers(event_handlers);
  
  style = styl;
  x = xp;
  y = yp;

  cont = 0;
  while (!cont) {
    input_loop();
    display_refresh();
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
  }
  input_loop_set_handlers(event_handlers_prev); // Give handlers back
  led_clear();
  return(cont);
}

// this isn't actually used anywhere, but..
void cell_reset (input_cell_t *prototype, short *val) {
  *val = prototype->value;
}

void render_cell (input_cell_t *prototype, short *val, int x, int y, text_style_t *styl ) {
  if (prototype == NULL) return;
  // this check could be anywhere, but let's put it here...
  if ((*val < prototype->min) || (*val > prototype->max)) *val = prototype->value;
  char str[prototype->length+1];
  switch (prototype->type) {
    case 1:
      sprintf(str, "%0*d",prototype->length,*val);
      draw_str(x,y,str,0,prototype->length,styl);
      break;
    case 2:
      draw_char(x,y,prototype->chars[*val-prototype->min],styl);
      break;
    case 3:
      draw_str(x,y,prototype->strings[*val-prototype->min],0,prototype->length, styl);
      break;
    default:
      break;
  }
}

// load cell content (value) from a string (character)
int load_input_cell (input_cell_t *prototype, short *val, char *string) {
  int i;
  short valu;
  // making sure we read only the right amount of chars
  char str[prototype->length+1];
  for (i=0;i<prototype->length;i++) str[i] = *(string++);

  switch (prototype->type) {
    case 1:
      if (sscanf(str,"%hd%n", &valu, &i) != 1 || i != prototype->length) {
        return 1;
      }
      *val = (short) valu;
      break;
    case 2:
      valu = char_index(prototype->chars, str[0], prototype->max-prototype->min+1);
      if (valu == -1) return 1;
      *val = valu + prototype->min;
      break;
    case 3:
      valu = str_index(prototype->strings, str, prototype->max-prototype->min+1);
      if (valu == -1) return 1;
      *val = valu + prototype->min;
      break;
    default:
      break;
  }
  return 0;
}

// helper functions for input (called by input_loop()), modifying
// the static variables declared on top of this file
static void input_cell_up (void) {
  if((*value)+1<=prot->max)
    (*value)++;
  render_cell(prot, value, x, y, style);
  scroll_bar(*value-prot->min, prot->max-prot->min, 12);
}

static void input_cell_down (void) {
  if((*value)-1>=prot->min)
    (*value)--;
  render_cell(prot, value, x, y, style);
  scroll_bar(*value-prot->min, prot->max-prot->min, 12);
}

static void input_cell_up_big (void) {
  if((*value)+10<=prot->max)
    (*value) += 10;
  render_cell(prot, value, x, y, style);
  scroll_bar(*value-prot->min, prot->max-prot->min, 12);
}

static void input_cell_down_big (void) {
  if((*value)-10>=prot->min)
    (*value) -= 10;
  render_cell(prot, value, x, y, style);
  scroll_bar(*value-prot->min, prot->max-prot->min, 12);
}

static void input_cell_next (void) {
  cont = 1;
}

static void input_cell_prev (void) {
  cont = -1;
}
