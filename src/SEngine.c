/*******************************************************************
  
  SEngine.c  - the Simple Engine, for playing uncomplicated sounds

  This file contains the procedure for playing a single row from the
  music data defined in SEngine_ui.c

 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "mzapo_phys.h"
#include "mzapo_regs.h"


#include "engine.h"
#include "SEngine.h"

static unsigned char *pwm_mem_base = NULL;

static void SEngine_init (void) {
  pwm_mem_base = map_phys_address(AUDIOPWM_REG_BASE_PHYS, AUDIOPWM_REG_SIZE, 0);
  if (pwm_mem_base == NULL)
    exit(1);
}

void SEngine_reset_pwm (void) {
  *(volatile uint32_t*)(pwm_mem_base + AUDIOPWM_REG_PWMPER_o) = 0;
  *(volatile uint32_t*)(pwm_mem_base + AUDIOPWM_REG_PWM_o) = 0;
}

void SEngine_play_row (short *values) {
  if (pwm_mem_base == NULL) SEngine_init();

  if(values[0]==-1) return;
  // decode period, this is probably the easiest implementation i could figure out
  int tone_indexes[] = {1,3,5,6,8,10,12};
  // i know
  uint32_t periods[] = {
    404954,   // B, to take care of Cb
    382226,   // C/B#
    360773,   // C#/Db
    340524,   // E/Fb
    321412,   // ......
    303373,
    286346,
    270274,
    255105,
    255105,
    227273,
    214517,
    202477,
    191113    // C (for B#)
  };

  uint32_t period = periods[tone_indexes[values[0]]+values[1]];
  if (values[2]<0)
    period <<= -values[2];
  else
    period >>= values[2];
  uint32_t width  = ((double) period) * values[3]/32.0;
  // set to short but audible pulse when 0
  if(!width) width = 2048;
  // write to audio output
  *(volatile uint32_t*)(pwm_mem_base + AUDIOPWM_REG_PWMPER_o) = period;
  *(volatile uint32_t*)(pwm_mem_base + AUDIOPWM_REG_PWM_o) = width;
}
