/*******************************************************************
  
  grid_edit.c  - simple field grid editor

  This file contans the user interface for full-fledged grid editing
  cell-by-cell, used while editing songs

 *******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "serialize_lock.h"
#include "mzapo_phys.h"
#include "font_types.h"

#include "graphics.h"
#include "knobs.h"
#include "input_loop.h"
#include "input_cell.h"
#include "input_field.h"
#include "field_grid.h"



// static helper procedures and variables for input_loop
static int editing = 0; // Indicates 1 next cell -1 prev cell (for usage in fields)

static int r, c, row;
field_grid_t *grid;
int right, down, edit;


static void grid_edit_end (void) {
  editing = 0; 
}

static void grid_edit_up (void) {
  down = -1;
}

static void grid_edit_down (void) {
  down = +1;
}

static void grid_edit_left (void) {
  right = -1;
}

static void grid_edit_right (void) {
  right = +1;
}

static void grid_edit_edit (void) {
  edit = 1;
}


static event_handler_t event_handlers[12] = {
  NULL, NULL, &grid_edit_down, &grid_edit_up,
  &grid_edit_end, NULL, NULL, NULL,
  &grid_edit_edit, NULL, &grid_edit_right, &grid_edit_left
};

void field_grid_edit (field_grid_t *target_grid, int x, int y, int start_row) {
  short selection[2];
  struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 20 * 1000 * 1000};
  event_handler_t event_handlers_prev[12];
  row = start_row;
  r = 0;
  c = 0;
  grid = target_grid;
  if (grid==NULL||grid->values==NULL) return; // IDK, if these checks are neccessary
  input_loop_get_handlers(event_handlers_prev); // Save present input handlers
  input_loop_set_handlers(event_handlers);
  editing = 1;
  while (editing) {
    edit = right = down = 0;
    input_loop();
    
    if (right) {
      c += right;
      if (c>=grid->nfields) {
        if (row+r+1<grid->nrows) {
          down = 1;
          c = 0;
        } else {
          c = grid->nfields-1;
        }
      } else if (c<0) {
        if (row+r-1>=0) {
          down = -1;
          c = grid->nfields-1;
        } else {
          c = 0;
        }
      }
    }
    if (down) {
      r += down;
      if (r>=grid->height) {
        if (row+grid->height < grid->nrows) row++;
        r = grid->height-1;
      }
      else if (r<0) {
        if (row>0) row--;
        r = 0;
      }
    }
    

    selection[0] = row + r;
    selection[1] = c;

    render_grid(grid, x, y, row, 1, selection);
    
    if (edit) {
      int x_offset = x + (grid->counting ? grid->fields[grid->nfields].width + grid->h_gap: 0);
      int c_offset = 0;
      int cp;
      for (cp=0;cp<c;cp++) {
        x_offset += grid->fields[cp].width + grid->h_gap;
        c_offset += grid->fields[cp].ncells;
      }
      grid->fields[c].values = grid->values + (row + r)*grid->ncells + c_offset;
      get_input_field(grid->fields+c, x_offset, y+r*grid->v_step);
    }

    display_refresh();
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
  }
  input_loop_set_handlers(event_handlers_prev); // Give handlers back
}


