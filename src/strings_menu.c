/*******************************************************************
  
  strings_menu.c  - just a menu

  This file uses the complicated field_grid interface just to make
  a simple text menu, consisting of an array of strings

 *******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "font_types.h"
#include "graphics.h"
#include "input_loop.h"
#include "input_cell.h"
#include "input_field.h"
#include "field_grid.h"
#include "grid_edit.h"
#include "str_utils.h"


static int move = 0;
static int choose = 0;


static void menu_up (void) {
  move = -1;
}

static void menu_down (void) {
  move = 1;
}


static void menu_select (void) {
  choose = 1;
}

static void menu_exit (void) {
  choose = -1;
}

static event_handler_t event_handlers[12] = {
  NULL, NULL, NULL, NULL,
  &menu_exit, NULL, NULL, NULL,
  &menu_select, NULL, &menu_down, &menu_up
};

// self-explanatory?
// grid, field, cells are provided by caller, mainly for styling reasons
int strings_menu(int x, int y, char **strings, int n,
    field_grid_t *grid) {

  int i;
  struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 20 * 1000 * 1000};
  int row = 0;
  int entry = 0;
  event_handler_t event_handlers_prev[12];
  
  short *values = malloc(n*sizeof(short));
  
  input_loop_get_handlers(event_handlers_prev);
  input_loop_set_handlers(event_handlers);
 
  for (i=0;i<n;i++) {
    values[i] = i;
  }
  display_refresh();
  //input_cell_t prot = {3, 0, n-1, 0, max_width, "", string};
  grid->fields->cells->type = 3;
  grid->fields->cells->min = 0;
  grid->fields->cells->max = n-1;
  // length of the cell is predetermined
  grid->fields->cells->strings = strings;
  
  //input_field_t field = {1, 0, 0, 10, 5, 2, 4, &prot, NULL, styles};
  grid->fields->ncells = 1;
  grid->fields->width = 0;  // we let render_field do the work
  grid->fields->height = 0;
  // padding etc is left what it is (hopefully something)

  //field_grid_t grid = {1, 1, height, n, 0, 5, 10, 0, 0, &field, values};

  grid->nfields = 1;
  grid->ncells = 1;
  // height is left
  grid->nrows = n;
  grid->v_step = 0;
  grid->counting = 0;
  grid->values = values;
  
  choose = 0;
  entry = 0;
  move = 0;

  while (choose==0) {
    short selected[2] = {0,0};
    input_loop();
    if ((entry+move>=0)&&(entry+move<n))
      entry += move;
    move = 0;
    if (entry<row) row--;
    if (entry>row+grid->height-1) row++;

    selected[0] = entry;
    render_grid(grid,x,y,row,1,selected);
    scroll_bar(entry,n-1,12);
    display_refresh();
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
  }
  led_clear();

  input_loop_set_handlers(event_handlers_prev); // Gib back
  free(values);
  if (choose==-1) return -1;
  return entry;
}
