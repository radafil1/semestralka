/*******************************************************************
  knobs.c     - implementation of functions declared in knobs.h
  
  Basic functions for processing MZ_APO knobs inputs, used in input
  loop, not really anywhere else, because input_loop takes care
  of physical addressing
 *******************************************************************/


#include "knobs.h"
#include "mzapo_regs.h"

knobs_t knobs_read (unsigned char *mem_base) {
  return *(volatile knobs_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
}

uint32_t knobs_to_uint (knobs_t knobs) {
  knob2int_t k;
  k.knobs = knobs;
  return k.value;
}

knobs_t uint_to_knobs (uint32_t knobs) {
  knob2int_t k;
  k.value = knobs;
  return k.knobs;
}

uint32_t knobs_comp (knobs_t k1, knobs_t k2) {
  knob2int_t k;
  uint32_t uint1;
  k.knobs = k1;
  uint1 = k.value;
  k.knobs = k2;
  return uint1^k.value;
}
