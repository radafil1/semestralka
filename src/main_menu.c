/*******************************************************************
  
  main_menu.c  - main menu the user sees upon running the program

  Program implements the main menu with engine selection and opening
  a desired music file

 *******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "font_types.h"
#include "graphics.h"
#include "input_loop.h"
#include "input_cell.h"
#include "input_field.h"
#include "field_grid.h"
#include "grid_edit.h"
#include "str_utils.h"
#include "file_utils.h"

#include "strings_menu.h"

#include "engine.h"


#define N_OPTIONS 3

static int entry = 0;
static char string[] = "New song\0Open song\0Exit\0";

// styles for the menu (see inc/graphics.h)
static text_style_t styles[3] = {
   {0xFDDF, 0xF0A6, 0xF00F, 0x01, 0x01, 0x01, 3, &font_winFreeSystem14x16},
   {0x0FF0, 0xF026, 0xF52F, 0x01, 0x00, 0x01, 3, &font_winFreeSystem14x16},
   {0xFFFF, 0xFD56, 0x2FF3, 0x01, 0x01, 0x01, 3, &font_winFreeSystem14x16}};


static void engines_menu (void) {
  int e;
  char back[] = "Back\0";
  char **avail_engines = (char**) malloc((ENGINES_N+1)*sizeof(char*));
  
  if (avail_engines==NULL)
    exit(1);
   
  for (e=0;e<ENGINES_N;e++) {
    avail_engines[e] = engines[e].name;
  }
  // for some reason strings_menu doesn't like reusing the same grid for different
  // menus
  input_cell_t prot_cpy = {3, 0, 0, 0, 10, NULL, NULL};
  input_field_t field_cpy = {1, 10, 0, 0, 10, 5, 2, 4, &prot_cpy, NULL, styles};
  field_grid_t grid_cpy = {1, 1, 3, 3, 0, 5, 10, 0, 0, &field_cpy, NULL};

  avail_engines[ENGINES_N] = back;
  
  display_clear();
  draw_str(50,14,"Sound Engine",0,-1,styles+2);
  
  e = strings_menu(20, 70, avail_engines, ENGINES_N+1, &grid_cpy);
  
  if (e==ENGINES_N||e<0) return;
  engines[e].engine_start();
  free(avail_engines);
}

// opening a music file
static void load_track (char *path) {
  FILE *fp;
  int e;
  char line0[256];

  fp = fopen(path, "r");
  if (fp==NULL)
    return;
  // we trust the engine to close the file whenever it wants to
  fread_line(fp, line0); // first line of file is name of engine
  for (e=0; e<ENGINES_N; e++) {
    if (str_comp(engines[e].name, line0)) {
      engines[e].engine_open_file(fp); // found the engine
      return;
    }
  }

  fclose(fp);
  display_clear();
  draw_str(50,120, "Unknown filetype", 0, -1, styles+2);
  display_refresh();
  wait_for_click();
}

// menu for user to choose a music file to load
static void open_track () {
  int files, filen, path_len;
  char *path;
  char *string = get_dir(MUSIC_DIR, &files);
  char **stringss;

  input_cell_t prot_cpy = {3, 0, 0, 0, 10, NULL, NULL};
  input_field_t field_cpy = {1, 10, 0, 0, 10, 5, 2, 4, &prot_cpy, NULL, styles};
  field_grid_t grid_cpy = {1, 1, 3, 3, 0, 5, 10, 0, 0, &field_cpy, NULL};
  if (files<1) {
    display_clear();
    free(string);
    draw_str(50,120, "Could not open music directory", 0, -1, styles+2);
    wait_for_click();
    return;
  }

  stringss = make_str_array(string,files);
  display_clear();
  draw_str(50,14,"Choose file",0,-1,styles+2);
  filen = strings_menu(20, 70, stringss, files, &grid_cpy);
  
  if (filen==-1) goto free_open_track; // I had to (at least once)
  path_len = char_index_str(stringss[filen],'\0') + char_index_str(MUSIC_DIR,'\0')-1;
  path = malloc(path_len*sizeof(char));
  if (path==NULL)
    exit(1);

  str_copy(path, MUSIC_DIR);
  // here, we're using char_index_str to find length (see src/str_utils.c)
  str_copy(path+char_index_str(MUSIC_DIR,'\0'), stringss[filen]);
  load_track(path);
  
  free(path);
free_open_track:
  free(string);
  free(stringss);
}


// the actual main menu :o
void main_menu () {
  input_cell_t prot = {3, 0, 0, 0, 10, NULL, NULL}; // for width
  input_field_t field = {1, 10, 0, 0, 10, 5, 2, 4, &prot, NULL, styles};
  field_grid_t grid = {1, 1, 3, 0, 0, 5, 10, 0, 0, &field, NULL};

  char **strings = make_str_array(string,3);
  display_clear();
  draw_str(50,14,"Main Menu",0,-1,styles+2);
  while (entry != 2 && entry != -1) {
    entry = strings_menu(20, 70, strings, 3, &grid);
    if (entry==0) {
      engines_menu();
      display_clear();
      draw_str(50,14,"Main Menu",0,-1,styles+2);
    } else if (entry==1) {
      open_track();
      display_clear();
      draw_str(50,14,"Main Menu",0,-1,styles+2);
    }
  }
  free(strings);
  display_clear();
  draw_str(50,120, "Goodbye", 0, -1, styles+2);
  display_refresh();
  display_free();
}
