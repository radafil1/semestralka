/*******************************************************************
  
  input_cell.h  - the smallest building stone of user input

  An input cell (defined here as a datatype) is the smallest input
  (and output) unit there is. It's contents can by described by a
  single short int value, which can mean either a numerical value,
  or an index into an array of characters or strings, functions
  are described in input_cell.c

 *******************************************************************/

#ifndef SEM_INPUT_CELL_H
#define SEM_INPUT_CELL_H

typedef struct {
  short type; // 1 number (range), 2 chracter (enum), 3 strings
  short min, max; // min and max of values, if type is some kind of array, 
                  // actual value - min is used as index
  short value;  // default value
  short length; // length of the cell in characters
  char *chars;  // array of chars for type 2
  char **strings; // array of possible strings (null terminated)
                // if type 2, first string is interpreted as list of possible chars (null terminated)

} input_cell_t;

int get_input_cell (input_cell_t *prototype, short *value,
                     int xp, int yp, text_style_t *styl);

void render_cell (input_cell_t *prototype, short *val, int x, int y, text_style_t *styl);
int load_input_cell (input_cell_t *prototype, short *val, char *string);
void reset_cell (input_cell_t *prototype, short *val);


#endif
