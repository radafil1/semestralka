/*******************************************************************
  
  graphics.h  - style type declaration and greaphical ui functions

  This file contains mainly the text_style_t datatype definition,
  the functions for working with the LCD and LED line are defined
  in src/graphics.c

 *******************************************************************/

#ifndef SEM_GRAPHICS_H
#define SEM_GRAPHICS_H
#include "font_types.h"

// style datatype for displaying text with background and scaling
typedef struct {
  unsigned short color0;    // fg
  unsigned short color1;    // bg
  unsigned short color2;    // border or smth idk
  unsigned char opacity0;
  unsigned char opacity1; 
  unsigned char opacity2;
  unsigned char size;       // Size multiplier
  font_descriptor_t* fdes;
} text_style_t;

void display_init (void);
void display_free (void);

void display_refresh (void);
void display_clear (void);

void draw_pixel (int x, int y, unsigned short color);
void draw_rect (int x, int y, int width, int height, unsigned short color);
int char_width(font_descriptor_t* fdes, int ch);


// bg < 0 for opaque
void draw_char(int x, int y, char ch, text_style_t *style);

// wrap 0 doesn't wrap
void draw_str (int x, int y, const char *str, unsigned int wrap,
               int len, text_style_t *style);


void scroll_bar (int current, int total, int width);
void progress_bar (int current, int total);
void led_clear (void);
#endif
