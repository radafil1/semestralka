/*******************************************************************
  knobs.h     - header file for reading knobs value
  
  File containing a simple structure for reading the values of MZ_APO
  knobs. Functions are implemented in knobs.c
 *******************************************************************/

#ifndef SEM_KNOBS_H
#define SEM_KNOBS_H

#include <stdint.h>

// Structured knobs inputs
typedef struct {
  unsigned int b    : 8;  // knob values (blue, green, red)
  unsigned int g    : 8;
  unsigned int r    : 8;
  unsigned int bc   : 1;  // knobs pressed (1 or 0)
  unsigned int gc   : 1;
  unsigned int rc   : 1;
  unsigned int empty: 5;  // offset
} knobs_t;

// Union used for knobs_t -> int conversion for comparison
// might not be the best solution, but it works
typedef union {
  knobs_t   knobs;
  uint32_t  value;
} knob2int_t;

knobs_t   knobs_read  (unsigned char *mem_base);

uint32_t  knobs_to_uint   (knobs_t knobs);
knobs_t   uint_to_knobs   (uint32_t knobs);

// Compare (returns 0 if equal!!)
uint32_t knobs_comp (knobs_t k1, knobs_t k2);

#endif
