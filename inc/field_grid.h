/*******************************************************************
  
  field_grid.h  - a table vertically-oriented table of fields

  This is the definition of the field_grid_t datatype and some
  useful general functions to go with it

 *******************************************************************/

#ifndef SEM_FIELD_GRID_H
#define SEM_FIELD_GRID_H

#include "input_field.h"
#include "input_cell.h"

/*

   The field grid is really just a group of fields, accompanied by
   some addititional information for rendering (gaps between fields,
   additional counting field to display row number)

   It holds the pointer to all the data it stores, but the general
   functions don't take care of it (they don't reallocate or free it)

*/

typedef struct {
  short nfields; // number of fields (horizontally)
  short ncells;  // number of cells in a row, for indexing values
  short height;  // number of displayed field rows
  short nrows;  // number of fields (non-empty)
  int maxrows; // maximum number of rows (won't extend further)
  short h_gap, v_gap; // horizontal and vertical gap between fields
  short v_step;  // used when rendering, height of highest field + v_gap,
                 // set to 0 to let it be calculated automatically
  short counting; // when not 0, fields[ncells] is used as a readonly field
                  // with the line number (printed before other fields)
  input_field_t *fields; // field prototype list
  short *values; // array of all values
} field_grid_t;

// I'd like to justify the use of the short type in many places.
// I really think, that in most places, if we'd use more than 10
// for the values, it wouldn't make any sense, so in order to save
// at least some memory I'm trying to put shorts wherever possible,
// but it's not really consistent, so future me, look into this pls..



void grid_default (field_grid_t *grid, int row);
void render_grid (field_grid_t *grid, int xp, int yp, int row,
  unsigned short nselected, short *selected);

int load_grid (field_grid_t *grid, int row, FILE *fp, int skip);

#endif
