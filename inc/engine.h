/*******************************************************************  
  
  engine.h    - engine type definition

  New engines should be added to the engine list in src/engines.c

*******************************************************************/

#ifndef SEM_ENGINE_H
#define SEM_ENGINE_H

#include <stdio.h>
#include <stdlib.h>

#define ENGINES_N 1

// all we need to know about an engine now
typedef struct {
  char * name;         /* checked against first line of music file */
  void (*engine_open_file)(FILE *fp);
  void (*engine_start)(void);
  // maybe add a general system for managing engine properties? (too much time :/)
} engine_t;

extern engine_t engines[ENGINES_N];


#endif /* ENGINE */
