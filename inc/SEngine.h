#ifndef SEM_SENGINE_H
#define SEM_SENGINE_H

// number of values per row
#define SENGINE_ROW_WIDTH 5

#include <stdio.h>

void SEngine_open (FILE *fp);
void SEngine_new  (void);

void SEngine_reset_pwm(void);
void SEngine_play_row (short *values);
void SEngine_play (short *rows, int nrows, unsigned int bpm);

#endif
