#ifndef SEM_INPUT_LOOP_H
#define SEM_INPUT_LOOP_H

#include "knobs.h"

typedef void (*event_handler_t)(void);

knobs_t input_knobs_read (void);

void input_loop_get_handlers (event_handler_t *event_handlers);
void input_loop_set_handlers (event_handler_t *event_handlers);

void wait_for_click (void);
void input_loop (void);

#endif
