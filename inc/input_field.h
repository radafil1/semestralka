/*******************************************************************
  
  input_field.h  - a group of cells

  This file contains the definition of the input_field_t datatype
  and some functions to work with it (for more info about them look
  into src/input_field.c)

 *******************************************************************/

#ifndef SEM_INPUT_FIELD_H
#define SEM_INPUT_FIELD_H

#include "input_cell.h"


// Horizontal input field, ie. a group of cells, which is displayed as a grid component
typedef struct {
  int ncells; // number of cells
  int length; // length of field in chars
  short width, height;  // Width and height in pixels (for easier grid position calculation)
  short padding_h; // We're not gonna reimplement the entirety of css are we?
  short padding_v;
  short border_width;
  short space;         // spacing between individual cells (in px, for readibility reasons)
  input_cell_t *cells; // Array of cell prototpes
  short *values; // array of values (what these values represent is determined by cell types)
  text_style_t *styles; // styles when selected or not selected, for use in a grid system
                        // indexes 0 - not selected, 1 - selected field, 2 - active cell
} input_field_t;
/* The styling system might be a bit confusing...
 * There is in total 4 colors we need to render a field: 
 *      text color, cell (text bg) color, field bg color and border color
 * since the individual cells don't have a border, we take the field border
 * from style 3 color3. So the colors in the 3 styles are used as follows:
 * 
 * (field unselected)
 * style 0: 
 *  color0: text
 *  color1: cell bg
 *  color2: field bg
 *
 * (field selected)
 * style 1: 
 *  color0: text
 *  color1: cell bg
 *  color2: field bg
 *
 * (selecting cell)
 * style 2: 
 *  color0: text
 *  color1: cell bg
 *  color2: border color (for all selections)
*/


int get_input_field (input_field_t *field, int x, int y);
void render_field (input_field_t *field, int x, int y, int selection);

int load_input_field (input_field_t *field, char *string);

void get_field_dimensions (input_field_t *field);

#endif
