#ifndef SEM_STR_UTILS_H
#define SEM_STR_UTILS_H

int has_extension ( char *filename, char *extension);

int str_comp (char *s1, char *s2);

int str_copy(char *dest, char *src);

int char_index_str (char *array, char ch);

int char_index (char *array, char ch, int n);

int str_index (char **strings, char *str, int n);

char **make_str_array(char *string, int n);

#endif
