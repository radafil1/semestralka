#ifndef SEM_FILE_UTILS_H
#define SEM_FILE_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

extern char *MUSIC_DIR;

char *get_dir (char *dir, int *num);

int fread_line (FILE *fp, char *buff);

#endif
