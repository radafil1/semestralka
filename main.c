/*******************************************************************
  main.c      - main program for the Very Simple Tracker

  Program just checks, if it got a directory with music and if yes,
  calls the main_menu() function from src/main_menu.c
 *******************************************************************/
 
#define _POSIX_C_SOURCE 200112L
#include <stdio.h>

#include "serialize_lock.h"
#include "main_menu.h"
#include "engine.h"

char *MUSIC_DIR;

int main(int argc, char *argv[]) {

  if (serialize_lock(1) <= 0) {
    printf("System is occupied\n");
    if (1) {
      printf("Waitting\n");
      /* Wait till application holding lock releases it or exits */
      serialize_lock(0);
    }
  }
  if (argc<=1) {
    printf("Couldn't find working directory\n");
    exit(0);
  }

  MUSIC_DIR = argv[1];
  main_menu();
}
